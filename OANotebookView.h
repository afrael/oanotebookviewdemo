/*
 OANotebookView.h
 Notebook

 This software, whether in the form of source code or executable, is provided 
 As Is. 
 
 There is no warranty that the software is appropriate for any specific use.  
 
 While it is believed to be correct, there is no guarantee that the code does 
 not contain either general errors or errors that my occur under specific 
 combination of parameters or use.
 
 Created by Afrael Ortiz on 7/13/11.
 This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 
 Unported License. To view a copy of this license, 
 visit http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to 
 Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 
 94041, USA.
 
 2011 Obelisk Apps.
 
*/

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#define kDefaultPaperTexture @"paper"
#define kDefaultCornerRadius 8.0f
#define kDefaultTextureImageSize 24.0f
#define kDefaultHorizontalLineColor [UIColor colorWithRed:0/255.0 green:124.0/255.0 blue:255.0/255.0  alpha:0.5]
#define kDefaultVerticalLineColor [UIColor colorWithRed:255.0/255.0 green:142.0/255.0 blue:142.0/255.0 alpha:0.5]
#define kDefaultHorizontalLineXOrigin 10.0
#define kDefaultHorizontalLineYOrigin 40.5
#define kDefaultHorizontalLineTopmostReference 80
#define kDefaultHorizontalLineBetweenSpace 40
#define kDefaultVerticalLineXOrigin kDefaultHorizontalLineYOrigin
#define kDefaultVerticalLineYOrigin 2.0

@interface OANotebookView : UIView{
    
}

- (UIImage *) patternImageOfSize:(CGSize) size;
- (void) setBackgroundTexture;
- (void) drawHorizonalBlueLines: (CGContextRef) ctx;
- (void) drawVerticalMarginLine;

@end
