
/*
  OANotebookView.m
  Notebook

 This software, whether in the form of source code or executable, is provided 
 As Is. 
 
 There is no warranty that the software is appropriate for any specific use.  
 
 While it is believed to be correct, there is no guarantee that the code does 
 not contain either general errors or errors that my occur under specific 
 combination of parameters or use.
 
 Created by Afrael Ortiz on 7/13/11.
 This work is licensed under the Apache License, Version 2.0.
 
 2011 Obelisk Apps.
 
*/

#import "OANotebookView.h"

@implementation OANotebookView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.opaque = NO;
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]){
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;    
}

- (void)drawRect:(CGRect)rect
{        
    NSLog(@"drawRect");
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(ctx, [UIColor clearColor].CGColor);
    CGContextFillRect(ctx, self.bounds);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds 
                                               byRoundingCorners:UIRectCornerAllCorners     
                                                     cornerRadii:CGSizeMake(kDefaultCornerRadius, kDefaultCornerRadius)];
    [path addClip];
    [self setBackgroundTexture];
    [self drawHorizonalBlueLines: ctx];
    [self drawVerticalMarginLine];
    [super drawRect:rect];
}

- (UIImage *) patternImageOfSize:(CGSize) size{
    UIImage *image = [UIImage imageNamed:kDefaultPaperTexture];
    UIGraphicsBeginImageContext(size);
    if((image.size.height * image.size.width) > (size.height * size.width)){
        [image drawInRect:CGRectMake(0.0, 0.0, size.width, size.height)];
    }
    UIGraphicsEndImageContext();
    return image;
}

- (void) setBackgroundTexture{
    UIImage *paperPatternImage = [self patternImageOfSize:CGSizeMake(kDefaultTextureImageSize, kDefaultTextureImageSize)];
    [[UIColor colorWithPatternImage:paperPatternImage] setFill];
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.bounds];
    [path fill];
}

- (void) drawHorizonalBlueLines: (CGContextRef) ctx{
    CGContextSaveGState(ctx);
    CGRect viewRect = self.bounds;
    CGFloat viewWidth = viewRect.size.width;    
    UIColor *lineColor = kDefaultHorizontalLineColor;
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(kDefaultHorizontalLineXOrigin, kDefaultHorizontalLineYOrigin)];
    [path addLineToPoint:CGPointMake(viewWidth, kDefaultHorizontalLineYOrigin)];
    [path setLineWidth:1.0];
    [lineColor setStroke];
    [path stroke];
    int numberOfLines = (viewRect.size.height - kDefaultHorizontalLineTopmostReference) / kDefaultHorizontalLineBetweenSpace ;
    for (int i = 0; i < numberOfLines; i++) {
        CGContextTranslateCTM(ctx, 0.0, (kDefaultHorizontalLineBetweenSpace + i));
        [path stroke];
    }
    CGContextRestoreGState(ctx);
}

- (void) drawVerticalMarginLine{
    CGRect viewRect = self.bounds;
    CGFloat viewHeight = viewRect.size.height;    
    viewHeight -= 2;
    UIColor *lineColor = kDefaultVerticalLineColor;
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(kDefaultVerticalLineXOrigin, kDefaultVerticalLineYOrigin)];
    [path addLineToPoint:CGPointMake(kDefaultVerticalLineXOrigin, (viewHeight -2))];
    [path setLineWidth:1.0];
    [lineColor setStroke];
    [path stroke];
    [path moveToPoint:CGPointMake(kDefaultVerticalLineXOrigin + 2.5, kDefaultVerticalLineYOrigin)];
    [path addLineToPoint:CGPointMake(kDefaultVerticalLineXOrigin + 2.5, (viewHeight -2))];
    [path stroke];
}

@end
